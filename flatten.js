function flatten(elements) {

    let newArray = [];

    function recursion(arr){
        
        for (let i = 0; i < arr.length;i++){          

            if (typeof arr[i] === 'object'){
                recursion(arr[i])
            }else {
                newArray.push(arr[i])
            }
        }
    }
    recursion(elements);
    return newArray
}
module.exports = flatten

