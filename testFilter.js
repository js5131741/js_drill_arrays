const array = require('./array')
const filter = require('./filter')

const result = filter(array, function(item){
    return item % 2 !== 0
})

console.log(result)