const array = require('./array')
const reduce = require('./reduce')

const sum = reduce(array, function(acc, curr){
    return acc + curr
}, 0)

console.log(sum)