const array = require('./array')
const each = require('./each')

each(array, function(item, index){
    console.log(`Squared value of item in index ${index} is ${item ** 2}`)
})