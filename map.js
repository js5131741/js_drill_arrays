function map(elements, cb) {
    const newarray = [];
    for(let index = 0; index < elements.length ; index++){
        newarray.push(cb(elements[index]));
    }
    return newarray;
}

module.exports = map