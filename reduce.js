function reduce(elements, cb, startingValue) {

    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    let acc = (startingValue === undefined) ? elements[0] : startingValue

    let startingIndex = (startingValue === undefined )? 1 : 0

    for(startingIndex; startingIndex < elements.length ; startingIndex++){
        // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
        acc = cb(acc, elements[startingIndex])
    }
    return acc
}

module.exports = reduce