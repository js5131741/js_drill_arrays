const array = require('./array')
const map = require('./map')

const squaredValues = map(array, function(item){
    return item *= item
})

console.log(squaredValues)