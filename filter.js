function filter(elements, cb){

    const newArray = []
    for(let index = 0; index < elements.length; index++){
        if(cb(elements[index])){
            newArray.push(elements[index])
        }
    }
    if (newArray.length === 0){
        return 'No output'
    } else{
        return newArray
    }
}

module.exports = filter